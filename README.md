todo app with Desktop Compose UI.

Libraries used:
- Jetpack Compose - shared UI
- [Decompose](https://github.com/arkivanov/Decompose) - navigation and lifecycle
- [MVIKotlin](https://github.com/arkivanov/MVIKotlin) - presentation and business logic
- [Reaktive](https://github.com/badoo/Reaktive) - background processing and data transformation
- [Exposed](https://github.com/JetBrains/Exposed) - data storage

### Running desktop application
 * To run, launch command: `./gradlew :run`


