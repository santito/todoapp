package dev.santito.main.store

import com.arkivanov.mvikotlin.core.store.Store
import dev.santito.main.TodoItem
import dev.santito.main.store.MainStore.Intent
import dev.santito.main.store.MainStore.State

internal interface MainStore : Store<Intent, State, Nothing> {

    sealed class Intent {
        data class SetItemDone(val id: Long, val isDone: Boolean) : Intent()
        data class DeleteItem(val id: Long) : Intent()
        data class SetText(val text: String) : Intent()
        object AddItem : Intent()
    }

    data class State(
        val items: List<TodoItem> = emptyList(),
        val text: String = ""
    )
}