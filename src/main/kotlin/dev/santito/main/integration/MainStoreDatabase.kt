package dev.santito.main.integration

import com.badoo.reaktive.completable.Completable
import com.badoo.reaktive.observable.Observable
import com.badoo.reaktive.observable.mapIterable
import dev.santito.database.TodoItemEntity
import dev.santito.database.ObservedDatabase
import dev.santito.main.TodoItem
import dev.santito.main.store.MainStoreProvider

internal class MainStoreDatabase(
    private val database: ObservedDatabase
) : MainStoreProvider.Database {

    override val updates: Observable<List<TodoItem>> =
        database
            .observeAll()
            .mapIterable { it.toItem() }

    private fun TodoItemEntity.toItem(): TodoItem =
        TodoItem(
            id = id,
            order = orderNum,
            text = text,
            isDone = isDone
        )

    override fun setDone(id: Long, isDone: Boolean): Completable =
        database.setDone(id = id, isDone = isDone)

    override fun delete(id: Long): Completable =
        database.delete(id = id)

    override fun add(text: String): Completable =
        database.add(text = text)
}