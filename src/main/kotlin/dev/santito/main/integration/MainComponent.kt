package dev.santito.main.integration

import com.arkivanov.decompose.ComponentContext
import com.arkivanov.decompose.value.Value
import com.arkivanov.decompose.value.operator.map
import com.arkivanov.mvikotlin.core.store.StoreFactory
import com.badoo.reaktive.base.Consumer
import com.badoo.reaktive.base.invoke
import dev.santito.database.ObservedDatabase
import dev.santito.main.Main
import dev.santito.main.Main.Model
import dev.santito.main.Main.Output
import dev.santito.main.store.MainStore.Intent
import dev.santito.main.store.MainStoreProvider
import dev.santito.utils.asValue
import dev.santito.utils.getStore

class MainComponent(
    componentContext: ComponentContext,
    storeFactory: StoreFactory,
    database: ObservedDatabase,
    private val output: Consumer<Output>
) : Main, ComponentContext by componentContext {

    private val store =
        instanceKeeper.getStore {
            MainStoreProvider(
                storeFactory = storeFactory,
                database = MainStoreDatabase(database = database)
            ).provide()
        }

    override val models: Value<Model> = store.asValue().map(stateToModel)

    override fun onItemClicked(id: Long) {
        output(Output.Selected(id = id))
    }

    override fun onItemDoneChanged(id: Long, isDone: Boolean) {
        store.accept(Intent.SetItemDone(id = id, isDone = isDone))
    }

    override fun onItemDeleteClicked(id: Long) {
        store.accept(Intent.DeleteItem(id = id))
    }

    override fun onInputTextChanged(text: String) {
        store.accept(Intent.SetText(text = text))
    }

    override fun onAddItemClicked() {
        store.accept(Intent.AddItem)
    }
}