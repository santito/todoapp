package dev.santito.main.integration

import dev.santito.main.Main.Model
import dev.santito.main.store.MainStore.State

internal val stateToModel: (State) -> Model =
    {
        Model(
            items = it.items,
            text = it.text
        )
    }