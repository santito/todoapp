package dev.santito.edit.store

import com.arkivanov.mvikotlin.core.store.Store
import dev.santito.edit.TodoItem
import dev.santito.edit.store.EditStore.Intent
import dev.santito.edit.store.EditStore.Label
import dev.santito.edit.store.EditStore.State

internal interface EditStore : Store<Intent, State, Label> {

    sealed class Intent {
        data class SetText(val text: String) : Intent()
        data class SetDone(val isDone: Boolean) : Intent()
    }

    data class State(
        val text: String = "",
        val isDone: Boolean = false
    )

    sealed class Label {
        data class Changed(val item: TodoItem) : Label()
    }
}