package dev.santito.edit

internal data class TodoItem(
    val text: String,
    val isDone: Boolean
)