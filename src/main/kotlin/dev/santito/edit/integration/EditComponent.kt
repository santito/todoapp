package dev.santito.edit.integration

import com.arkivanov.decompose.ComponentContext
import com.arkivanov.decompose.value.Value
import com.arkivanov.decompose.value.operator.map
import com.arkivanov.mvikotlin.core.store.StoreFactory
import com.badoo.reaktive.base.Consumer
import com.badoo.reaktive.base.invoke
import dev.santito.database.ObservedDatabase
import dev.santito.edit.Edit
import dev.santito.edit.Edit.Model
import dev.santito.edit.Edit.Output
import dev.santito.edit.store.EditStore.Intent
import dev.santito.edit.store.EditStoreProvider
import dev.santito.utils.asValue
import dev.santito.utils.getStore

class EditComponent(
    componentContext: ComponentContext,
    storeFactory: StoreFactory,
    database: ObservedDatabase,
    itemId: Long,
    private val output: Consumer<Output>
) : Edit, ComponentContext by componentContext {

    private val store =
        instanceKeeper.getStore {
            EditStoreProvider(
                storeFactory = storeFactory,
                database = EditStoreDatabase(database = database),
                id = itemId
            ).provide()
        }

    override val models: Value<Model> = store.asValue().map(stateToModel)

    override fun onTextChanged(text: String) {
        store.accept(Intent.SetText(text = text))
    }

    override fun onDoneChanged(isDone: Boolean) {
        store.accept(Intent.SetDone(isDone = isDone))
    }

    override fun onCloseClicked() {
        output(Output.Finished)
    }
}