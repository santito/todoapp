package dev.santito.edit.integration

import com.badoo.reaktive.completable.Completable
import com.badoo.reaktive.maybe.Maybe
import com.badoo.reaktive.maybe.map
import dev.santito.database.TodoItemEntity
import dev.santito.database.ObservedDatabase
import dev.santito.edit.TodoItem
import dev.santito.edit.store.EditStoreProvider.Database


internal class EditStoreDatabase(
    private val database: ObservedDatabase
) : Database {

    override fun load(id: Long): Maybe<TodoItem> =
        database
            .select(id = id)
            .map { it.toItem() }

    private fun TodoItemEntity.toItem(): TodoItem =
        TodoItem(
            text = text,
            isDone = isDone
        )

    override fun setText(id: Long, text: String): Completable =
        database.setText(id = id, text = text)

    override fun setDone(id: Long, isDone: Boolean): Completable =
        database.setDone(id = id, isDone = isDone)
}