package dev.santito.edit.integration

import dev.santito.edit.Edit.Model
import dev.santito.edit.store.EditStore.State

internal val stateToModel: (State) -> Model =
    {
        Model(
            text = it.text,
            isDone = it.isDone
        )
    }