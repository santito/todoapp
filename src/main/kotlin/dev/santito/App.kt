package dev.santito

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import androidx.compose.ui.window.rememberWindowState
import com.arkivanov.decompose.ComponentContext
import com.arkivanov.decompose.DefaultComponentContext
import com.arkivanov.decompose.ExperimentalDecomposeApi
import com.arkivanov.decompose.extensions.compose.jetbrains.lifecycle.LifecycleController
import com.arkivanov.essenty.lifecycle.LifecycleRegistry
import com.arkivanov.mvikotlin.main.store.DefaultStoreFactory
import com.badoo.reaktive.coroutinesinterop.asScheduler
import com.badoo.reaktive.scheduler.overrideSchedulers
import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.sqlite.driver.JdbcSqliteDriver
import dev.santito.database.SQLObservedDatabase
import dev.santito.database.Database
import dev.santito.root.Root
import dev.santito.root.integration.RootComponent
import dev.santito.ui.RootContent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.io.File

@OptIn(ExperimentalCoroutinesApi::class, ExperimentalDecomposeApi::class)
fun main() {
    overrideSchedulers(main = Dispatchers.Main::asScheduler)

    val lifecycle = LifecycleRegistry()
    val root = root(DefaultComponentContext(lifecycle = lifecycle))

    application {
        val windowState = rememberWindowState()
        LifecycleController(lifecycle, windowState)

        Window(
            onCloseRequest = ::exitApplication,
            state = windowState,
            title = "TodoApp"
        ) {
            Surface(modifier = Modifier.fillMaxSize()) {
                RootContent(component = root)
            }
        }
    }
}

fun sqlDriver(): SqlDriver {
    val databasePath = File("build/database.db")
    val driver = JdbcSqliteDriver(url = "jdbc:sqlite:${databasePath.canonicalFile.absolutePath}")
    Database.Schema.create(driver)

    return driver
}

private fun root(componentContext: ComponentContext): Root =
    RootComponent(
        componentContext = componentContext,
        storeFactory = DefaultStoreFactory(),
        database = SQLObservedDatabase(sqlDriver())
    )