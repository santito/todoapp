package dev.santito.root

import com.arkivanov.decompose.router.stack.ChildStack
import com.arkivanov.decompose.value.Value

interface Root {

    val childStack: Value<ChildStack<*, Child>>

    sealed class Child {
        data class Main(val component: dev.santito.main.Main) : Child()
        data class Edit(val component: dev.santito.edit.Edit) : Child()
    }
}
