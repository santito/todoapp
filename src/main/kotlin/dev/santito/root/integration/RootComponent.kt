package dev.santito.root.integration

import com.arkivanov.decompose.ComponentContext
import com.arkivanov.decompose.router.stack.ChildStack
import com.arkivanov.decompose.router.stack.StackNavigation
import com.arkivanov.decompose.router.stack.childStack
import com.arkivanov.decompose.router.stack.pop
import com.arkivanov.decompose.router.stack.push
import com.arkivanov.decompose.value.Value
import com.arkivanov.essenty.parcelable.Parcelable
import com.arkivanov.essenty.parcelable.Parcelize
import com.arkivanov.mvikotlin.core.store.StoreFactory
import com.badoo.reaktive.base.Consumer
import dev.santito.database.ObservedDatabase
import dev.santito.edit.Edit
import dev.santito.edit.integration.EditComponent
import dev.santito.main.Main
import dev.santito.main.integration.MainComponent
import dev.santito.root.Root
import dev.santito.root.Root.Child
import dev.santito.utils.Consumer


class RootComponent internal constructor(
    componentContext: ComponentContext,
    private val main: (ComponentContext, Consumer<Main.Output>) -> Main,
    private val edit: (ComponentContext, itemId: Long, Consumer<Edit.Output>) -> Edit
) : Root, ComponentContext by componentContext {

    constructor(
        componentContext: ComponentContext,
        storeFactory: StoreFactory,
        database: ObservedDatabase
    ) : this(
        componentContext = componentContext,
        main = { childContext, output ->
            MainComponent(
                componentContext = childContext,
                storeFactory = storeFactory,
                database = database,
                output = output
            )
        },
        edit = { childContext, itemId, output ->
            EditComponent(
                componentContext = childContext,
                storeFactory = storeFactory,
                database = database,
                itemId = itemId,
                output = output
            )
        }
    )

    private val navigation = StackNavigation<Configuration>()

    private val stack =
        childStack(
            source = navigation,
            initialConfiguration = Configuration.Main,
            handleBackButton = true,
            childFactory = ::createChild
        )

    override val childStack: Value<ChildStack<*, Child>> = stack

    private fun createChild(configuration: Configuration, componentContext: ComponentContext): Child =
        when (configuration) {
            is Configuration.Main -> Child.Main(main(componentContext, Consumer(::onMainOutput)))
            is Configuration.Edit -> Child.Edit(edit(componentContext, configuration.itemId, Consumer(::onEditOutput)))
        }

    private fun onMainOutput(output: Main.Output): Unit =
        when (output) {
            is Main.Output.Selected -> navigation.push(Configuration.Edit(itemId = output.id))
        }

    private fun onEditOutput(output: Edit.Output): Unit =
        when (output) {
            is Edit.Output.Finished -> navigation.pop()
        }

    private sealed class Configuration : Parcelable {
        @Parcelize
        object Main : Configuration()

        @Parcelize
        data class Edit(val itemId: Long) : Configuration()
    }
}
