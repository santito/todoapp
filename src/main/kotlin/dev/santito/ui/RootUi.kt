package dev.santito.ui

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import com.arkivanov.decompose.ExperimentalDecomposeApi
import com.arkivanov.decompose.extensions.compose.jetbrains.stack.Children
import com.arkivanov.decompose.extensions.compose.jetbrains.stack.animation.fade
import com.arkivanov.decompose.extensions.compose.jetbrains.stack.animation.plus
import com.arkivanov.decompose.extensions.compose.jetbrains.stack.animation.scale
import com.arkivanov.decompose.extensions.compose.jetbrains.stack.animation.stackAnimation
import dev.santito.root.Root
import dev.santito.root.Root.Child
import dev.santito.theme.DarkColors
import dev.santito.theme.LightColors

@Composable
@OptIn(ExperimentalDecomposeApi::class)
fun RootContent(
    useDarkTheme: Boolean = isSystemInDarkTheme(),
    component: Root
) {
    val colors = if (!useDarkTheme) {
        LightColors
    } else {
        DarkColors
    }

    MaterialTheme (colorScheme = colors){
        Children(
            stack = component.childStack,
            animation = stackAnimation(fade() + scale()),
        ) {
            when (val child = it.instance) {
                is Child.Main -> MainContent(child.component)
                is Child.Edit -> EditContent(child.component)
            }
        }
    }
}