import org.jetbrains.compose.desktop.application.dsl.TargetFormat

plugins {
    kotlin("jvm")
    id("org.jetbrains.compose")
    id("com.squareup.sqldelight") version "1.5.4"
}

sqldelight {
    database("Database") {
        packageName = "dev.santito.database"
    }
}

repositories {
    google()
    mavenCentral()
    maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
}

dependencies {
    implementation(compose.desktop.currentOs)
    implementation(compose.material3)
    val mviKotlin = "3.0.0"
    implementation("com.arkivanov.mvikotlin:rx:$mviKotlin")
    implementation("com.arkivanov.mvikotlin:mvikotlin:$mviKotlin")
    implementation("com.arkivanov.mvikotlin:mvikotlin-main:$mviKotlin")
    implementation("com.arkivanov.mvikotlin:mvikotlin-extensions-reaktive:$mviKotlin")
    val decompose = "1.0.0-alpha-05"
    implementation("com.arkivanov.decompose:decompose:$decompose")
    implementation("com.arkivanov.decompose:extensions-compose-jetbrains:$decompose")
    val reaktive = "1.2.1"
    implementation("com.badoo.reaktive:reaktive:$reaktive")
    implementation("com.badoo.reaktive:coroutines-interop:$reaktive")
    val sqlDeLight = "1.5.4"
    implementation("com.squareup.sqldelight:sqlite-driver:$sqlDeLight")
    val coroutines = "1.6.4"
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-swing:$coroutines")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines")

    implementation("com.arkivanov.essenty:lifecycle:0.6.0")
}



compose.desktop {
    application {
        mainClass = "dev.santito.AppKt"
        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageName = "TodoApp"
            packageVersion = "1.0.0"
        }
    }
}
